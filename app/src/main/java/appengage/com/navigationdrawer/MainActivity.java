package appengage.com.navigationdrawer;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnLayoutChangeListener {
    public static final String KEY_JS_BRIDGE_CONSTANT = "Android";

    final String[] data = {"one", "two", "three"};
    final String[] fragments = {
            "appengage.com.navigationdrawer.my_fragments.FragmentOne",
            "appengage.com.navigationdrawer.my_fragments.FragmentTwo",
            "appengage.com.navigationdrawer.my_fragments.FragmentThree"};

    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ActionBarDrawerToggle toggle;

    private TextView drawerHeaderTV;

    private WebInterface webInterface;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler(Looper.getMainLooper());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Hello World");

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                try {
                    showWebViewDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Log.d("nitin", "onDrawerSlide():::" + slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                Log.d("nitin", "onDrawerOpened()");
                //getSupportActionBar().setTitle("onDrawerOpened");
                //invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                Log.d("nitin", "onDrawerClosed()");
                //getSupportActionBar().setTitle("onDrawerClosed");
                //invalidateOptionsMenu();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                Log.d("nitin", "onDrawerStateChanged()");
            }
        };
        drawer.setDrawerListener(toggle);
//        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawerHeaderTV = (TextView) navigationView.findViewById(R.id.textView);

        navigationView.setNavigationItemSelectedListener(this);

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[0]));
        tx.addToBackStack("somename");
        tx.commit();
    }

    private void showWebViewDialog() throws Exception {
        //AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.RED));
        LinearLayout wrapper = new LinearLayout(MainActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        wrapper.setLayoutParams(layoutParams);

        webInterface = new WebInterface(MainActivity.this, dialog);


        WebView webViewInstance = new WebView(MainActivity.this);
        EditText keyboardHack = new EditText(MainActivity.this);

        keyboardHack.setVisibility(View.GONE);

        webViewInstance.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webViewInstance.addJavascriptInterface(webInterface, KEY_JS_BRIDGE_CONSTANT);
        webViewInstance.getSettings().setJavaScriptEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            webViewInstance.getSettings().setAllowFileAccessFromFileURLs(true);
            webViewInstance.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }
        webViewInstance.getSettings().setAllowContentAccess(true);
        webViewInstance.getSettings().setAllowFileAccess(true);

        webViewInstance.getSettings().setAppCacheEnabled(false);
        webViewInstance.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webViewInstance.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webViewInstance.loadUrl("http://feedback.webengage.com/f/311c463b");
        //webViewInstance.loadUrl("http://www.webengage.com");

//        String layoutHtmlFileContents = getLayoutHtmlFileContents("feedback_form.html");
//        webViewInstance.loadDataWithBaseURL("file:///android_asset/", layoutHtmlFileContents, "text/html", "utf-8", null);

        webViewInstance.requestFocus(View.FOCUS_DOWN);
        webViewInstance.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                if (url.indexOf("/widget/publisher/feedback.html") > -1) {
//                    view.loadUrl("document.getElementsByClassName('feedback-win-cancel')[0].innerText = 'hello';");
//                }
                //dialog.getWindow().getDecorView().invalidate();
                Log.d("APP_WEBVIEW", "onPageFinished()");
                super.onPageFinished(view, url);
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();
                Log.d("webview", "in shouldInterceptRequest()");
                if (url.indexOf("callback-helper.js") > -1) {
                    Log.d("webview", "requested: callback-helper.js");
                    String localCallbackHelper = loadAssetTextAsString(MainActivity.this, "callback-helper.js");
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(localCallbackHelper.getBytes()));
                } else {
                    return super.shouldInterceptRequest(view, request);
                }

            }
        });

        webViewInstance.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onCloseWindow(WebView window) {
                Log.d("APP_WEBVIEW", "onCloseWindow()");
                finish();
                //super.onCloseWindow(window);
            }

            public void onCloseWindow(Window w) {
                Log.d("APP_WEBVIEW", "onCloseWindow()");
                finish();
                //super.onCloseWindow(window);
            }
        });

        wrapper.setOrientation(LinearLayout.VERTICAL);
        wrapper.addView(webViewInstance, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        wrapper.addView(keyboardHack, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(wrapper);
        dialog.show();
        Log.d("rwv", "wefwe");

//        alert.setView(wrapper);
//        alert.show();

//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                dialog.dismiss();
//            }
//        }, 3000);
    }

    private String loadAssetTextAsString(Context context, String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = context.getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ((str = in.readLine()) != null) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.d("nitin", "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.d("nitin", "Error closing asset " + name);
                }
            }
        }

        return null;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
//            isPlaying = false;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                this.invalidateOptionsMenu();
//            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

//        if (isPlaying) {
//            mPlayMenuItem.setVisible(false); // hide play button
//            mPauseMenuItem.setVisible(true); // show the pause button
//        } else if (!isPlaying) {
//            mPlayMenuItem.setVisible(true); // show play button
//            mPauseMenuItem.setVisible(false); // hide the pause button
//        }

        return true;
    }

    // "View.OnLayoutChangeListener"
    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        Log.d("nitin", "onLayoutChange()");
        navigationView.removeOnLayoutChangeListener(this);
        //drawerHeaderTV.setText("New title...");
    }

    // "NavigationView.OnNavigationItemSelectedListener"
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        try {
            String resourceEntryName = getResources().getResourceEntryName(id);
            drawerHeaderTV.setText(resourceEntryName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_camera) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[0]));
        } else if (id == R.id.nav_gallery) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[1]));
        } else if (id == R.id.nav_slideshow) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[2]));
        } else if (id == R.id.nav_manage) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[0]));
        } else if (id == R.id.nav_share) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[1]));
        } else if (id == R.id.nav_send) {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[2]));
        } else {
            tx.replace(R.id.main, Fragment.instantiate(MainActivity.this, fragments[0]));
        }

        tx.addToBackStack("somename");
        tx.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
