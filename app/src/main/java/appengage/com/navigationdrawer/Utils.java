package appengage.com.navigationdrawer;

import android.util.Log;

public class Utils {
    public static String getCurrentMethodName() {
        return Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static void myLog(){
        myLog("");
    }
    public static void myLog(String extreMessage){
        Log.d("nitin", getCurrentMethodName() + ":::[" + extreMessage + "]");
    }
}
