package appengage.com.navigationdrawer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebInterface {
    Activity mActivity;
    Context mContext;
    Dialog dialog;

    WebInterface(Activity activity, Dialog dialog) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        this.dialog = dialog;
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Log.d("APP_WEBVIEW", "showToast() called");
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void onLoaded() {
        Log.d("FORM", "onLoaded() called");
    }

    @JavascriptInterface
    public void feedbackLoad(String data) {
        Log.d("APP_WEBVIEW", "showToast() called");
        Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void handleTransportData(String callbackFrameName, String eventName, String pageUrl, String data) {
        Log.d("APP_WEBVIEW", "handleTransportData() called");
        Log.d("APP_WEBVIEW", "callbackFrameName, eventName, pageUrl, data: " + callbackFrameName + " ::: " + eventName + " ::: " + pageUrl + " ::: " + data);
    }

    @JavascriptInterface
    public void closeWindow() {
        Log.d("APP_WEBVIEW", "closeWindow() called");
        //this.mActivity.finish();
        this.dialog.dismiss();
    }
}
